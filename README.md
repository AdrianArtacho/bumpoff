# BumpOff #

This M4L device receives messages (typically from the [**Rocket**](https://bitbucket.org/AdrianArtacho/rocket/src/master/)
device) depending on the instance number: in order to receive messages from *Rocket1*, both that *rocket* instance
and the corresponding *BumpOff* instance need to be set to __the same number__.

![repo:R107:BumpOff](https://docs.google.com/drawings/d/e/2PACX-1vQuzDFioEFLPuvGGzD3gpi3Cly5SKALb3l59LHo4TRF6FkqlGb_TSeveIxRYxjZyrgTSXhsfDS6uTQ6/pub?w=425&h=393)

### Output ###

This device outputs 4 CC streams, corresponding to 

* CC22 (Rotation in [**Rocket**](https://bitbucket.org/AdrianArtacho/rocket/src/master/))
* CC23 (Midilight in [**Rocket**](https://bitbucket.org/AdrianArtacho/rocket/src/master/))
* CC24 (Reduction in [**Rocket**](https://bitbucket.org/AdrianArtacho/rocket/src/master/))
* CC25 (Opposite in [**Rocket**](https://bitbucket.org/AdrianArtacho/rocket/src/master/))

